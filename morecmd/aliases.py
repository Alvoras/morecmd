# -*- coding: utf-8 -*-

from . import helpers
import re


def load_aliases(app):
    methods = helpers.list_methods(app, prefix="do")

    for m in methods:
        # If there is at least two '_'
        # Get the probable mode '_[mode]_'
        # If mode exists in modes
        # Put the given aliases in the sub dict
        mode = 'base'
        m_func = getattr(app, m)
        docs = m_func.__doc__

        # Look for mode in function name
        if m.count('_') > 1:
            # Extract mode name
            buf_mode = m.split('_')[1]
            # If extracted mode is a registered mode, use it
            if m.split('_')[1] in app.modes:
                mode = buf_mode
        if docs is not None:
            # Parse docstring to get aliases linked to this function
            parsed_aliases = parse_aliases(docs)
            for alias in parsed_aliases:
                # If mode key does not exists, create it
                if mode not in app.aliases:
                    app.aliases[mode] = {}
                app.aliases[mode][alias] = m


def parse_aliases(docs):
    trimmed_aliases_split = []
    rgx = re.compile(r'alias[\ ]*{(.*)}')
    match = rgx.search(docs)
    if match:
        aliases = match.group(1)
        aliases_split = aliases.split(',')
        trimmed_aliases_split = [el.strip() for el in aliases_split]

    return trimmed_aliases_split
