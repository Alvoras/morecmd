# -*- coding: utf-8 -*-

from . import prompt

def set_mode(app, mode_name):
    app.selected_mode = mode_name
    prompt.set_prefix_prompt(app)

def clear_mode(app):
    app.selected_mode = ''
    app.prompt = app.base_prompt

def mode_not_found(mode):
    print(f"Unknown mode :  '{mode}'")
