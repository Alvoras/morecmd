# -*- coding: utf-8 -*-


def set_opt(opt_name, opt_val):

    # Inner function that handle the decorated function
    def decorator(func):

        # Wrapper which handle the passed args and the inner logic
        def wrapper(*args, **kwargs):
            # *args contains all the decorated function
            # arguments
            # the self instance is the first one
            app = args[0]
            func(*args, **kwargs)
            app.setup_args[opt_name] = opt_val
        return wrapper

    return decorator


def set_all_opt(values):

    def decorator(func):

        def wrapper(*args, **kwargs):
            app = args[0]
            func(*args, **kwargs)
            for key, val in values.items():
                app.setup_args[key] = val
        return wrapper

    return decorator
