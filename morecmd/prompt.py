# -*- coding: utf-8 -*-

def set_prompt(app, prompt):
    app.base_prompt = prompt
    app.prompt = app.base_prompt

def set_prefix_prompt(app):
    app.prompt = f"({app.selected_mode}){app.base_prompt}"

def clear_prefix_prompt(app):
    set_prefix_prompt(app)
