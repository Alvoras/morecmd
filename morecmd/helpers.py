# -*- coding: utf-8 -*-


def list_methods(obj, prefix="do"):
    return [func for func in dir(obj) if callable(getattr(obj, func)) and func.startswith(f"{prefix}_")]


def is_valid_builtin(cmd_args):
    ret = True
    parsed_args = cmd_args.split(' ')
    if len(parsed_args) > 1 or len(parsed_args[0]) is 0:
        ret = False

    return ret
