# -*- coding: utf-8 -*-
# from . import helpers
from . import aliases
from . import commands
from . import modes
from . import prompt
from cmd import Cmd
import signal
from collections import defaultdict
# import functools


class Morecmd(Cmd):
    """Main Morecmd class"""
    # Allow nested dicts
    aliases = defaultdict(dict)
    aliases["base"] = {}
    selected_mode = ''
    modes = []
    prefix = ''
    setup_lock = False
    setup_args = defaultdict(dict)
    runnable = False
    setup_func_name = ''
    base_prompt = Cmd.prompt

    def __init__(self, default_prompt='> '):
        # Catch KeyboardInterrupt (^C)
        signal.signal(signal.SIGINT, self.sigint_handler)

        super(Morecmd, self).__init__()
        prompt.set_prompt(self, default_prompt)

    def preloop(self):
        aliases.load_aliases(self)

    def print_setup_lock(self):
        print("Can't change state in setup mode. Use 'exit' to exit this mode.")

    def get_function_name(self, name, ftype="do"):
        if self.selected_mode is not '':
            function_name = f"{ftype}_{self.selected_mode}_{name}"
        else:
            function_name = f"{ftype}_{name}"

        return function_name

    def print_setup_arg(self, arg_name, arg_val):
        print(f"+ {arg_name} : {arg_val}")

    # Leave load_modes here since it is called externally and using it
    # as modes.load_modes(app) would make it wierd
    def load_modes(self, modes):
        self.modes = modes

    # Overriding Cmd.onecmd to allow alias parsing
    def onecmd(self, line):
        cmd, arg, line = self.parseline(line)
        if not line:
            return self.emptyline()

        if cmd is None:
            return self.default(line)

        self.lastcmd = line

        # if line == 'EOF':
        #     self.lastcmd = ''

        return self.handle_cmd(cmd, arg, line)

    def handle_cmd(self, cmd, arg, line):
        if cmd == '':
            return self.default(line)

        function_name = self.get_function_name(cmd)

        try:
            func = getattr(self, function_name)
        except AttributeError:
            # if the command is not found in the "do_" functions then
            # Look at the aliases list
            # If the cmd is in the alias list,
            # then call the linked "do_" function
            # Else print error message
            mode = 'base'
            if self.selected_mode != '':
                mode = self.selected_mode

            if cmd in self.aliases[mode]:
                try:
                    func = getattr(self, self.aliases[mode][cmd])
                except AttributeError:
                    # Try to parse cmd as a cmd_function from commands.py
                    return self.default(line)
            else:
                try:
                    cmd_func = getattr(commands, f"cmd_{cmd}")
                    return cmd_func(self, cmd, arg, line)
                except AttributeError:
                    return self.default(line)

        return func(arg)

    def run(self, callback):
        if self.runnable:
            # Trigger the callback then reset the setup mode state
            callback(self)
            self.setup_lock = False
            self.toggle_runnable()
            self.setup_args = {}
            self.setup_func_name = ''
            modes.clear_mode(self)
        else:
            # Switch the runnable var to allow the callback to run
            # next time it is called
            self.toggle_runnable()

    def toggle_runnable(self):
        if self.runnable:
            self.runnable = False
        else:
            self.runnable = True

    def show_opt(self):
        print("Options available :")
        for opt_name, opt_val in self.setup_args.items():
            print('\t', end='')
            self.print_setup_arg(opt_name, opt_val)

    def sigint_handler(self, signal, frame):
        print("\nBye")
        exit(0)
