# -*- coding: utf-8 -*-

from . import helpers, modes


def cmd_hello():
    print("command hello")


def cmd_setup(app, cmd, arg, line):
    if helpers.is_valid_builtin(arg):
        # Fetch method list and corresponding setup function name
        methods = helpers.list_methods(app, prefix="setup")
        function_name = app.get_function_name(arg, ftype="setup")

        if function_name in methods:
            # If the setup function exists
            # Call the function once to trigger the decorators and thus
            # set up the functions args to given default values
            app.setup_lock = True
            app.setup_func_name = function_name
            modes.set_mode(app, arg)
            setup_func = getattr(app, function_name)
            return setup_func()

    return app.default(line)


def cmd_show(app, cmd, arg, line):
    if app.setup_lock:
        return app.show_opt()
    else:
        return app.default(line)


def cmd_run(app, cmd, arg, line):
    if app.setup_lock:
        if not helpers.is_valid_builtin(arg):
            setup_func = getattr(app, app.setup_func_name)
            return setup_func()

    return app.default(line)


def cmd_set(app, cmd, arg, line):
    if app.setup_lock and arg.count(' ') > 0:
        arg_name, arg_val = arg.split(' ', 1)

        if arg_name in app.setup_args:
            app.setup_args[arg_name] = arg_val
            return app.print_setup_arg(arg_name, app.setup_args[arg_name])

    return app.default(line)


def cmd_use(app, cmd, arg, line):
    if app.setup_lock:
        return app.print_setup_lock()

    if not helpers.is_valid_builtin(arg):
        modes.mode_not_found(app, arg)
    else:
        if arg in app.modes:
            print(f"Mode {arg} selected")
            return modes.set_mode(app, arg)
        else:
            return modes.mode_not_found(app, arg)


def cmd_quit(app, cmd, arg, line):
    print("Bye")
    exit(0)


def cmd_exit(app, cmd, arg, line):
    if app.selected_mode != '':
        if app.setup_lock:
            app.setup_lock = False
            app.runnable = False
            app.setup_args = {}
            app.setup_func_name = ''
        return modes.clear_mode(app)
    else:
        print("Bye")
        exit(0)


def cmd_EOF(app, cmd, arg, line):
    print('\n', end='')
    cmd_exit(app, cmd, arg, line)
