# -*- coding: utf-8 -*-
from morecmd import *
from morecmd.decorators import set_opt, set_all_opt

class Labo(Morecmd):
    """docstring for labo."""

    def __init__(self, prompt):
        super(Labo, self).__init__(prompt)
#        self.set_prompt(prompt)

    def do_hello(self, args):
        """
        alias{foo}
        """
        print("hello")

    def do_get_foo(self, args):
        """
        alias{bar}
        """
        print("foo")

    @set_all_opt({
    "foo":"bar",
    "baz":"qux"
    })
    def setup_foo(self):
        def foo(self):
            print("SETUP_FOO HAS BEEN CALLED")
            print(self.setup_args)

        self.run(foo)

if __name__ == '__main__':
    app = Labo(prompt="> ")
    app.load_modes(["get", "set"])

    app.cmdloop('Welcome !')
