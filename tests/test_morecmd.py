# -*- coding: utf-8 -*-

from .context import *


def test_hello(capsys):
    mock_app.do_foo()
    cap = capsys.readouterr()
    assert cap.out.strip() == "foo"


def test_get_function_name():
    res = mock_app.get_function_name(name="foo", ftype="do")
    assert res == "do_foo"
    res = mock_app.get_function_name(name="foo", ftype="setup")
    assert res == "setup_foo"


def test_show_opt(capsys):
    mock_app.setup_args = {"foo": "bar", "baz": "baz"}
    mock_app.show_opt()
    cap = capsys.readouterr()

    assert cap.out.rstrip() == "Options available :\n\t+ foo : bar\n\t+ baz : baz"


def test_toggle_runnable():
    assert mock_app.runnable is False
    mock_app.toggle_runnable()
    assert mock_app.runnable is True


def test_run(capsys):
    def mock_foo(self):
        print("foo")

    mock_app.runnable = False
    assert mock_app.runnable is False
    mock_app.run(mock_foo)
    assert mock_app.runnable is True
    mock_app.run(mock_foo)
    cap = capsys.readouterr()
    assert cap.out.rstrip() == "foo"
