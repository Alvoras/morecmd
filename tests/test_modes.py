# -*- coding: utf-8 -*-

from .context import *


def test_load_modes():
    mock_app.load_modes(["get"])
    assert mock_app.modes == ["get"]


def test_set_mode():
    set_mode(mock_app, "foo")
    assert mock_app.selected_mode == "foo"


def test_clear_mode():
    set_mode(mock_app, "foo")
    assert mock_app.prompt == "(foo)> "
    clear_mode(mock_app)
    assert mock_app.prompt == "> "
