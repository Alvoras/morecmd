# -*- coding: utf-8 -*-

from .context import *

def test_set_option(capsys):
    mock_app.setup_bar()
    capsys.readouterr()  #  Prevent the print
    assert mock_app.setup_args["foo"] == "bar"


def test_set_all_options(capsys):
    mock_app.setup_foo()
    capsys.readouterr()  #  Prevent the print
    assert mock_app.setup_args["foo"] == "bar"
    assert mock_app.setup_args["baz"] == "qux"
