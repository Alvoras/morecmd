# -*- coding: utf-8 -*-

from .context import *

def test_parse_aliases():
    assert aliases.parse_aliases(mock_app.do_foo.__doc__) == ['bar']


def test_load_aliases():
    aliases.load_aliases(mock_app)
    assert mock_app.aliases == {"base": {'bar': 'do_foo'}}
