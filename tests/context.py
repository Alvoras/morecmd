# -*- coding: utf-8 -*-

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from morecmd import Morecmd
from morecmd import helpers
from morecmd import commands
from morecmd import aliases
from morecmd.modes import *
from morecmd.decorators import *


class Mockup(Morecmd):

    def __init__(self, prompt):
        super(Mockup, self).__init__()

    def do_foo(self):
        """
        alias{bar}
        """
        print("foo")

    @set_all_opt({
        "foo": "bar",
        "baz": "qux"
    })
    def setup_foo(self):
        def foo(self):
            print("SETUP_FOO HAS BEEN CALLED")
            print(self.setup_args)

        self.run(foo)

    @set_opt("foo", "bar")
    def setup_bar(self):
        def bar(self):
            print("SETUP_BAR HAS BEEN CALLED")
            print(self.setup_args)

        self.run(bar)


mock_app = Mockup("> ")
