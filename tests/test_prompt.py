# -*- coding: utf-8 -*-

from .context import *

def test_set_prefix_prompt():
    mock_app.prompt = "> "
    mock_app.selected_mode = "foo"
    prompt.set_prefix_prompt(mock_app)
    assert mock_app.prompt == "(foo)> "
    mock_app.selected_mode = ''
