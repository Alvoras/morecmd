MoreCmd module repository
=========================

Builtin Cmd framework with added features
-----------------------

**Features** :
  + Aliases
  + Modes with specific nested methods
  + Setup mode

**ToDo** :
  + ✅ Aliases
  + ✅ Modes
  + ✅ Setup mode
  + ✅ Refactor ugly elif cascade
  + ✅ Split features into submodules
  + Suggestions welcomed

**Pondering** :
  + Nested modes

Aliases
-------
| You can set aliases for your methods using the pattern ``alias{foo}`` in the methods' docstring.
| Multiple aliases are supported.

Example : ``alias{foo, bar, baz}``

Modes
-----
A mode allows a user to run mode-specific methods when selected.

Method pattern :
  ``do_<mode>_<function_name>()``

A mode is selected with the command ``use <mode>`` and left with ``exit`` or ``EOF`` (ctrl+D).
When a mode is selected, the prompt will be prefixed with ``(<mode>)`` and the app functions are parsed with the ``do_<mode>_`` prefix.

**Note :**
 |  Setup functions support modes.
 |  Example pattern : ``setup_<mode>_<function_name>()``

Setup mode
----------
When running ``setup <function>``, allow to setup the function arguments before running the command

Method pattern :
  ``setup_<function_name>()``

Commands available :
  + ``show``
     + Print the available options and their values
  + ``set key val``
     + Set the ``key`` option with the ``val`` value
  + ``run``
     + Run the command with the given options

The options values are set with decorators :
  + ``@set_opt(key, val)``
  + ``@set_all_opt({"key1": val1, "key2": val2})``

You can access the options' content in your code with :
  ``self.setup_args[<option_name>]``
