all:
	@echo

test:
	clear && py.test -vv

lab:
	clear && python3 labo.py

run:
	clear && python3 labo.py
